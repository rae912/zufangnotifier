# -*- coding: utf-8 -*-

from elasticsearch import Elasticsearch
import MySQLdb
from settings import MYSQL

es = Elasticsearch(hosts='http://172.16.17.98/')

db = MySQLdb.connect(host=MYSQL['MYSQL_HOST'], user=MYSQL['MYSQL_USER'],
                     passwd=MYSQL['MYSQL_PASSWD'], db=MYSQL['MYSQL_DBNAME'], charset='utf8', use_unicode=True)
cursor = db.cursor()

sql = """SELECT * FROM %s""" % (MYSQL['MYSQL_TABLENAME'])
cursor.execute(sql)

result = cursor.fetchall()

for ele in result:
    timestamp = ele[2].isoformat() + "+08:00"

    doc = {
        "author": ele[3],
        "phoneNum": ele[4],
        "description": ele[5],
        "timestamp": timestamp,
        "url": ele[1],
    }

    res = es.index(index="scrapyd", doc_type='scrapy_item', body=doc)

# {
#     "query":
#         {"match_phrase":
#              {"description": "龙阳路"}},
#     "sort": [
#         {"timestamp": {"order": "asc"}}
#     ],
# }
