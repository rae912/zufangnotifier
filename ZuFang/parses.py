# coding=utf-8


# A decorator for solving UnicodeError
def codecheck(func):
    def wrapper(*args, **kwargs):
        result = ""
        content = func(*args, **kwargs)
        for word in content:
            temp = ""
            try:
                temp = word.encode("gbk")
            except UnicodeEncodeError:
                print("An unicodeEncodeError happened.")
            result += temp
        return result.decode("gbk")

    return wrapper


# Description
@codecheck
def description(response):
    content = ''
    descriptions = response.css(".topic-content>p::text").extract()
    for sentence in descriptions:
        content += sentence.strip()
    return content


# Author
@codecheck
def author(response):
    result = response.css('.from>a::text').extract_first()
    return result
