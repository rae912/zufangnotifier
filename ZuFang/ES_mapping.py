{
    "mappings": {
        "_default_": {
            "_all": {
                "enabled": false
            }
        },
        "scrapy_item": {
            "_timestamp": {
                "enabled": false
            },
            "properties": {
                "author": {
                    "type": "string"
                },
                "description": {
                    "fields": {
                        "cn": {
                            "analyzer": "ik",
                            "type": "string"
                        },
                        "en": {
                            "analyzer": "english",
                            "type": "string"
                        }
                    },
                    "index": "analyzed",
                    "type": "string"
                },
                "phoneNum": {
                    "type": "string"
                },
                "timestamp": {
                    "format": "date_optional_time",
                    "type": "date"
                },
                "url": {
                    "type": "string"
                }
            }
        }
    }
}
