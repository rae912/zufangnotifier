# !/usr/bin/python
# -*-coding:utf-8-*-

import random
import time


def random_sleep(factor=1):
    sleep_time = random.randint(20, 40)
    print('Sleep %s seconds' % (sleep_time * factor))
    time.sleep(sleep_time * factor)

