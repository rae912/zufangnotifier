# -*- coding: utf-8 -*-

import re
import sys

import MySQLdb
import scrapy
from scrapy import Request

import ZuFang.parses as formater
import sleep_util
from ZuFang.items import ZufangItem
from ZuFang.settings import MYSQL

reload(sys)
sys.setdefaultencoding('utf-8')

db = MySQLdb.connect(host=MYSQL['MYSQL_HOST'], user=MYSQL['MYSQL_USER'],
                     passwd=MYSQL['MYSQL_PASSWD'], db=MYSQL['MYSQL_DBNAME'], charset='utf8', use_unicode=True)
cursor = db.cursor()


class DbShZufang(scrapy.Spider):
    name = 'friend'
    custom_settings = {
        "ITEM_PIPELINES": {
        'ZuFang.pipelines.FriendPipeline': 300,
        },
        "DEFAULT_REQUEST_HEADERS": {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, sdch',
        'Accept-Language': 'zh-CN,zh;q=0.8',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Host': 'www.douban.com',
        'Pragma': 'no-cache',
        'Referer': 'http://www.douban.com/group/shanghaizufang/discussion?start=50',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36',
        }
    }
    start_urls = ['https://www.douban.com/group/216303/',
                  'https://www.douban.com/group/368133/',
                  'https://www.douban.com/group/133266/',
                  'https://www.douban.com/group/418155/',
                  'https://www.douban.com/group/486708/']

    def parse(self, response):
        items = response.css(".title>a").xpath("@href").extract()
        for topic_url in items:
            cursor.execute('SELECT url FROM friend WHERE url=\'%s\'' % topic_url)
            if_exist = cursor.fetchone()
            if 'sidebar' in topic_url:
                pass
            elif if_exist:
                print('HAVE BEEN VISITED!!!', topic_url)
            else:
                sleep_util.random_sleep(0.2)
                yield Request(url=topic_url, callback=self.parse_detail, encoding='utf-8')

        # next_page = response.css(".next>a").xpath("@href").extract_first()
        # sleep_util.random_sleep(0.3)
        # yield Request(url=next_page, callback=self.parse, encoding='utf-8')

    def parse_detail(self, response):
        item = ZufangItem()

        # Description
        content = formater.description(response)
        item['description'] = content

        # PhoneNumber
        digit = re.compile(r'\d+').findall(content)
        regex = re.compile(r'^0?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$')
        for number in digit:
            result = regex.match(number)
            if result:
                item['phoneNum'] = result.group()

        if item.get('phoneNum') is None:
            item['phoneNum'] = 'NULL'

        # Timestamp
        timestamp = response.css('.color-green::text').extract_first()
        item['timestamp'] = timestamp

        # Author
        item['author'] = formater.author(response)

        # URL
        item['url'] = response.url

        yield item

