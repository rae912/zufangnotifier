# -*- coding: utf-8 -*-

import scrapy
from scrapy import Request
from ZuFang.items import ZufangItem
import re
import MySQLdb
from ZuFang.settings import MYSQL
import sleep_util
import sys
import ZuFang.parses as formater

reload(sys)
sys.setdefaultencoding('utf-8')

db = MySQLdb.connect(host=MYSQL['MYSQL_HOST'], user=MYSQL['MYSQL_USER'],
                     passwd=MYSQL['MYSQL_PASSWD'], db=MYSQL['MYSQL_DBNAME'], charset='utf8', use_unicode=True)
cursor = db.cursor()


class DbShZufang(scrapy.Spider):
    name = 'douban_sh'
    start_urls = ['https://www.douban.com/group/shanghaizufang/discussion?start=0',  # 上海租房
                  'https://www.douban.com/group/homeatshanghai/discussion?start=0',  # 上海租房2
                  'https://www.douban.com/group/shzf/discussion?start=0',  # 上海直租
                  'https://www.douban.com/group/zufan/discussion?start=0',  # 长宁徐汇静安租房
                  'https://www.douban.com/group/583132/discussion?start=0',  # 上海无中介租房
                  "https://www.douban.com/group/pudongzufang/discussion?start=0",  # 浦东租房
                  "https://www.douban.com/group/467799/discussion?start=0",  # 房东直租
                  "https://www.douban.com/group/558784/discussion?start=0",  # 浦东个人租房
                  ]

    def parse(self, response):
        items = response.css(".title>a").xpath("@href").extract()
        for topic_url in items:
            cursor.execute('SELECT url FROM zufang WHERE url=\'%s\'' % topic_url)
            if_exist = cursor.fetchone()
            if 'sidebar' in topic_url:
                pass
            elif if_exist:
                print('HAVE BEEN VISITED!!!', topic_url)
            else:
                sleep_util.random_sleep(0.2)
                yield Request(url=topic_url, callback=self.parse_detail, encoding='utf-8')

        # next_page = response.css(".next>a").xpath("@href").extract_first()
        # sleep_util.random_sleep(0.3)
        # yield Request(url=next_page, callback=self.parse, encoding='utf-8')

    def parse_detail(self, response):
        item = ZufangItem()

        # Description
        content = formater.description(response)
        item['description'] = content

        # PhoneNumber
        digit = re.compile(r'\d+').findall(content)
        regex = re.compile(r'^0?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$')
        for number in digit:
            result = regex.match(number)
            if result:
                item['phoneNum'] = result.group()

        if item.get('phoneNum') is None:
            item['phoneNum'] = 'NULL'

        # Timestamp
        timestamp = response.css('.color-green::text').extract_first()
        item['timestamp'] = timestamp

        # Author
        item['author'] = formater.author(response)

        # URL
        item['url'] = response.url

        yield item

