# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import MySQLdb
from settings import MYSQL, KEYWORD
from WXsender import WXsender
import sys
from elasticsearch import Elasticsearch
import re

reload(sys)
sys.setdefaultencoding('utf-8')


class ZufangPipeline(object):
    def __init__(self):
       self.db = MySQLdb.connect(host=MYSQL['MYSQL_HOST'], user=MYSQL['MYSQL_USER'],
                            passwd=MYSQL['MYSQL_PASSWD'], db=MYSQL['MYSQL_DBNAME'], charset='utf8', use_unicode=True)
       self.cursor = self.db.cursor()

    def alert(self, dic):
        for word in KEYWORD:
            if word in dic['description']:
                # 2017年07月29日 15:08:48新增房价判断，小于或等于才发信息
                pattern = re.compile(r"\s*\d{4}")
                price = map(lambda x: int(x) if int(x) <= 2000 else False, pattern.findall(dic['description']))

                if [False] == list(set(price)):
                    return

                payload = {
                    'text': u'租房信息',
                    'desp': dic['timestamp'] + dic['description'] + ' ' + dic['url']
                }
                WXsender(payload)
                return

    def process_item(self, item, spider):
        if spider.name == 'douban_sh':
            # Send WX Notice
            self.alert(item)

            # Insert to MYSQL
            try:
                base_sql = 'INSERT INTO %s' % MYSQL['MYSQL_TABLENAME']
                self.cursor.execute(base_sql + """(url, timestamp, author, phoneNum, description) VALUES (%s, %s, %s, %s, %s)
                    """, (item['url'], item['timestamp'], item['author'], item['phoneNum'], item['description'])
                )
            except Exception:
                print(self.cursor._last_executed)

            for ele in (item['url'], item['timestamp'], item['author'], item['phoneNum'], item['description']):
                print ele

        self.db.commit()
        return item


class FriendPipeline(object):
    def __init__(self):
       self.db = MySQLdb.connect(host=MYSQL['MYSQL_HOST'], user=MYSQL['MYSQL_USER'],
                            passwd=MYSQL['MYSQL_PASSWD'], db=MYSQL['MYSQL_DBNAME'], charset='utf8', use_unicode=True)
       self.cursor = self.db.cursor()

    def alert(self, dic):
        for word in [u"男友", u"男朋友"]:
            if word in dic['description']:
                payload = {
                    'text': u'交友信息',
                    'desp': dic['timestamp'] + dic['description'] + ' ' + dic['url']
                }
                WXsender(payload)
                return

    def process_item(self, item, spider):
        if spider.name == 'friend':
            # Send WX Notice
            self.alert(item)

            # Insert to MYSQL
            try:
                base_sql = 'INSERT INTO friend'
                self.cursor.execute(base_sql + """(url, timestamp, author, phoneNum, description) VALUES (%s, %s, %s, %s, %s)
                    """, (item['url'], item['timestamp'], item['author'], item['phoneNum'], item['description'])
                )
            except Exception:
                print(self.cursor._last_executed)

            for ele in (item['url'], item['timestamp'], item['author'], item['phoneNum'], item['description']):
                print ele

        self.db.commit()
        return item


class ElasticSearchPipeline(object):
    def __init__(self):
        self.es = Elasticsearch(hosts='http://es00.chinacloudapp.cn/')

    def process_item(self, item, spider):
        temp = item['timestamp'].split(' ')
        timestamp = temp[0]+'T'+temp[1]+'+08:00'

        doc = {
            "author": item['author'],
            "phoneNum": item['phoneNum'],
            "description": item['description'],
            "timestamp": timestamp,
            "url": item['url'],
        }

        res = self.es.index(index="scrapyd", doc_type='scrapy_item', body=doc)

        return item