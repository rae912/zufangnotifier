# coding=utf-8

import commands
import json
from WXsender import WXsender

req_cmd = """curl -H 'Host: app.xwsd.com' -H 'Accept: */*' -H 'Cookie: PHPSESSID=dklvj5g4g3m5634gpus1njglp0' -H 'User-Agent: WeiLoan/1.2 CFNetwork/811.5.4 Darwin/16.6.0' -H 'Accept-Language: zh-cn' --compressed 'https://app.xwsd.com/api/crtrs?userSecret=cb323f969457b62d&sign=78a547f6e542dcfe5b6bc579315f9fbe&pageSize=20&userId=1000020218&page=1' """

(status, resp) = commands.getstatusoutput(req_cmd)

if status != 0: quit()

body = resp.split("\n")[-1]
items = json.loads(body)['data']['records']
for i in items:
    if int(i['remainDay']) < 300:
        text = u"{1}\n剩余金额{0}\n剩余天数{3}\n年化{2}%".format(i['moneyLast'], i['title'], i['oddYearRate'] * 100, i['remainDay'])
	print text
	WXsender(dict(title=u"投标提醒", desp=text))
